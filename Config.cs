using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

public class ClientProps
{
    public string ClientName { get; set; }
    public string ZipFileDirectory { get; set; }
    public string ZipFilePrefix { get; set; }
    public string ExtractDirectory { get; set; }
    public string DatabaseConnection { get; set; }
    public List<string> ClientTables { get; set; }
}

public class Config
{
    public List<ClientProps> Clients { get; set; }
    public bool Log { get; set; }

    public Config(string fileName)
    {
        XmlDocument xmlDocument = new XmlDocument();

        try
        {
            xmlDocument.Load(fileName);
        }
        catch
        {
        }

        try
        {
            #region Sample config
            //<?xml version="1.0" encoding="utf-8" ?>
            //<clients>
            //  <client name="Grant County">
            //    <zipFile directory="D:\Projects\39\LLLowImport\Data" prefix="Remittance" />
            //    <extractLocation directory="D:\temp\Test" />
            //    <database connection="Data Source=CCGIS03\SQLSERVER2005;Initial Catalog=test;User Id=test;Password=test;Connection Timeout=300" />
            //  </client>
            //</clients>
            #endregion

            Clients = new List<ClientProps>();

            XmlNodeList nodeList = xmlDocument.SelectNodes("/clients/client");
            XmlNode nodeLog = xmlDocument.SelectSingleNode("clients");

            if (nodeLog.Attributes["log"].Value.ToLower() == "true")
            {
                this.Log = true;
            }
            else
            {
                this.Log = false;
            }

            foreach (XmlNode nodeClient in nodeList)
            {
                string clientName = nodeClient.Attributes["name"].Value;

                XmlNode nodeZipFile = nodeClient.SelectSingleNode("zipFile");
                string zipFileDirectory = nodeZipFile.Attributes["directory"].Value;
                string zipFilePrefix = nodeZipFile.Attributes["prefix"].Value;
                XmlNode nodeExtract = nodeClient.SelectSingleNode("extractLocation");
                string extractDirectory = nodeExtract.Attributes["directory"].Value;
                XmlNode nodeDBConn = nodeClient.SelectSingleNode("database");
                string dbConnection = nodeDBConn.Attributes["connection"].Value;
                XmlNode nodeEmail = nodeClient.SelectSingleNode("database");
                XmlNodeList clientTableNodes = nodeClient.SelectNodes("tables/table");
                List<string> clientTables = new List<string>();

                foreach (XmlNode nodeClientTable in clientTableNodes)
                {
                    clientTables.Add(nodeClientTable.Attributes["name"].Value);
                }

                ClientProps clientProps = new ClientProps()
                {
                    ClientName = clientName,
                    ZipFileDirectory = zipFileDirectory,
                    ZipFilePrefix = zipFilePrefix,
                    ExtractDirectory = extractDirectory,
                    DatabaseConnection = dbConnection,
                    ClientTables = clientTables,
                };

                Clients.Add(clientProps);
            }
        }
        catch (Exception ex)
        {
            string c = ex.StackTrace;
        }
    }
}


