﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;

using Ionic.Zip; 

namespace LowImport
{
    class Installment
    {
        public string ChargeDescription { get; set; }
        public double ChargeAmount { get; set; }
        public double AmountPaid { get; set; }
        public double Balance { get; set; }
    }

    class CreditRate
    {
        public string CreditRateType { get; set; }
        public double CreditRateValue { get; set; }
    }

    class Credit
    {
        public string CreditType { get; set; }
        public double CreditAmount { get; set; }
    }

    class Deduction
    {
        public string DeductionType { get; set; }
        public double DeductionAmount { get; set; }
    }

    class Unit
    {
        public string UnitDescription { get; set; }
        public double UnitAmount { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string appDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).Replace("file:\\", "");
            string xmlPath = appDirectory + "\\Config.xml";
            Config config = new Config(xmlPath);
            StreamWriter writer = new StreamWriter(appDirectory + @"\log.txt", true);

            writer.WriteLine("");
            writer.WriteLine("Started import at " + DateTime.Now.ToString());

            foreach (ClientProps clientProps in config.Clients)
            {
                ProcessRemoteDirectory(clientProps, writer);
                ProcessClient(clientProps, writer);
            }

            writer.WriteLine("Finished import at " + DateTime.Now.ToString());
            writer.Close();

            if (config.Log)
            {
                SendEmail("no-reply@39dn.com", "denise.s@39dn.com", "LOW import log", "Hi Denise, please review the daily import log.", appDirectory + @"\log.txt");
            }
        }

        private static string SendEmail(string sender, string recipient, string subject, string body, string fileattachment)
        {
            string status = string.Empty;

            try
            {
                MailAddress sendFrom = new MailAddress(sender);
                MailAddress sendTo = new MailAddress(recipient);
                MailMessage message = new MailMessage(sendFrom, sendTo);
                
                message.Subject = subject;
                message.Body = body;

                Attachment attachFile = new Attachment(fileattachment);

                message.Attachments.Add(attachFile);

                SmtpClient emailClient = new SmtpClient();
                
                emailClient.Credentials = new System.Net.NetworkCredential("no-reply@39dn.com", "N0R3ply!");
                emailClient.Host = "smtp.gmail.com";
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.Send(message);

                status = "Success";
            }
            catch (SmtpException ex)
            {
                status = ex.Message;
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }

            return status;
        }

        private static void ProcessRemoteDirectory(ClientProps clientProps, StreamWriter writer)
        {
            if (Directory.Exists(clientProps.ZipFileDirectory))
            {
                string file;
                string[] fileEntries = Directory.GetFiles(clientProps.ZipFileDirectory);
                string latestFile = string.Empty;
                DateTime maxDate = DateTime.MinValue;
                DateTime writeDate = DateTime.MinValue;

                foreach (string filePath in fileEntries)
                {
                    file = Path.GetFileName(filePath).ToUpper();

                    if (file.ToUpper().StartsWith(clientProps.ZipFilePrefix.ToUpper()) && file.ToUpper().EndsWith(".ZIP"))
                    {
                        writeDate = File.GetLastWriteTime(filePath);
                      
                        if (writeDate > maxDate)
                        {
                            maxDate = writeDate;
                            latestFile = filePath;
                        }
                    }
                }

                if (latestFile != string.Empty)
                {
                    foreach (string filePath in fileEntries)
                    {
                        if (filePath != latestFile)
                        {
                            File.Delete(filePath);
                        }
                    }
                }
            }
        }

        private static void ProcessClient(ClientProps clientProps, StreamWriter writer)
        {
            SqlConnection sqlConnection = null;

            try
            {
                writer.WriteLine("");
                writer.WriteLine("Processing client " + clientProps.ClientName);
                Console.WriteLine("Processing client " + clientProps.ClientName);

                string zipFileDirectory = clientProps.ZipFileDirectory;
                string zipFilePrefix = clientProps.ZipFilePrefix;
                string databaseConnection = clientProps.DatabaseConnection;

                //Locate zip file
                string zipFilePath = string.Empty;

                foreach (string filePath in Directory.GetFiles(zipFileDirectory, zipFilePrefix + "*.ZIP"))
                {
                    string zipFileName = Path.GetFileName(filePath).ToUpper();

                    //if (zipFileName.StartsWith(zipFilePrefix.ToUpper()) && zipFileName.EndsWith(".ZIP"))
                    //{
                        zipFilePath = filePath;
                        break;
                    //}
                }

                if (zipFilePath == string.Empty)
                {
                    throw new Exception("Remittance zip file not found.");
                }

                //Truncate tables
                sqlConnection = new SqlConnection();
                sqlConnection.ConnectionString = databaseConnection;
                
                sqlConnection.Open();
               
                SqlCommand sqlCommand = sqlConnection.CreateCommand();
                
                foreach (string tableName in clientProps.ClientTables)
                {                    
                    sqlCommand.CommandText = "DELETE FROM " + tableName;                   
                    sqlCommand.ExecuteNonQuery();
                }

                //Extract text files
                string extractDirectory = clientProps.ExtractDirectory;

                using (ZipFile zipFile = ZipFile.Read(zipFilePath))
                {
                    foreach (ZipEntry entry in zipFile)
                    {
                        if (entry.FileName.ToUpper().EndsWith(".TXT"))
                        {
                            entry.Extract(extractDirectory, ExtractExistingFileAction.OverwriteSilently);
                        }
                    }
                }

                //Access text files
                if (Directory.Exists(extractDirectory))
                {
                    //string[] filePaths = Directory.GetFiles(extractDirectory);
                    string comparisonFilePath = string.Empty;
                    string remittanceFilePath = string.Empty;

                    /*
                    foreach (string filePath in filePaths)
                    {
                        if (filePath.ToUpper().Contains("COMPARISON "))
                        {
                            comparisonFilePath = filePath;
                        }

                        if (filePath.ToUpper().Contains("REMITTANCE "))
                        {
                            remittanceFilePath = filePath;
                        }
                    }
                    */

                    foreach (string filePath in Directory.GetFiles(extractDirectory, "remittance*"))
                    {
                        remittanceFilePath = filePath;                        
                    }

                    foreach (string filePath in Directory.GetFiles(extractDirectory, "comparison*"))
                    {
                        remittanceFilePath = filePath;
                    }

                    writer.WriteLine("Importing from files " + comparisonFilePath + " and " + remittanceFilePath);

                    string[] separator = new string[] { "\",\"" };
                    StreamReader reader = File.OpenText(remittanceFilePath);
                    string record = reader.ReadLine();
                    int count = 1;
                    string insert = string.Empty;
                    List<string> insertStatementList = new List<string>();
                    
                    while (record != null)
                    {
                        record = record.Substring(1, record.Length - 2);
                        string[] parameterArray = record.Split(separator, StringSplitOptions.None);

                        //ParcelInformationTax
                        if (clientProps.ClientTables.Contains("ParcelInformationTax"))
                        {
                            insert = "INSERT INTO ParcelInformationTax (PIN_18, TAX_10, OWNER, OWNER_STREET, OWNER_CITY_ST_ZIP, LEGAL_DESC, PROPERTY_CLASS) " +
                                      "VALUES ('" + parameterArray[5] + "','" + parameterArray[0] + "','" +
                                                    parameterArray[30].Replace("'", "''") + "','" + parameterArray[32].Replace("'", "''") + "','" +
                                                    parameterArray[34].Replace("'", "''") + "','" + parameterArray[24].Replace("'", "''") + "','" +
                                                    parameterArray[3].Replace("'", "''") + "')";

                            insertStatementList.Add(insert);
                        }

                        //ParcelTaxInstallments 
                        if (clientProps.ClientTables.Contains("ParcelTaxInstallments"))
                        {
                            List<Installment> installments = GetInstallmentsForParcel(parameterArray);
                            foreach (Installment installment in installments)
                            {
                                string amountPaid = installment.AmountPaid.ToString();
                                string balance = installment.Balance.ToString();

                                insert = "INSERT INTO ParcelTaxInstallments (PIN_18, TAX_10, TAXYEAR, TAXYEARDESCRIPTION, CHARGEDESCRIPTION, TAXSETDESCRIPTION, TAXRATE, CHARGEAMOUNT, AMOUNTPAID, BALANCE) " +
                                         "VALUES ('" + parameterArray[5] + "','" + parameterArray[0] + "','" +
                                                       parameterArray[2].Substring(0, 4) + "','" + parameterArray[2].Replace("'", "''") + "','" +
                                                       installment.ChargeDescription + "','" + parameterArray[6].Replace("'", "''") + "'," +
                                                       parameterArray[7] + "," + installment.ChargeAmount.ToString() + "," +
                                                       amountPaid + "," + balance + ")";

                                insertStatementList.Add(insert);
                            }
                        }

                        //ParcelTaxCreditRates 
                        if (clientProps.ClientTables.Contains("ParcelTaxCreditRates"))
                        {
                            List<CreditRate> creditRates = GetCreditRatesForParcel(parameterArray);
                            foreach (CreditRate creditRate in creditRates)
                            {
                                insert = "INSERT INTO ParcelTaxCreditRates (PIN_18, CREDITRATE, CREDITRATETYPE) " +
                                         "VALUES ('" + parameterArray[5] + "'," + creditRate.CreditRateValue.ToString() + ",'" +
                                                       creditRate.CreditRateType + "')";

                                insertStatementList.Add(insert);
                            }
                        }

                        //ParcelTaxCredits 
                        if (clientProps.ClientTables.Contains("ParcelTaxCredits"))
                        {
                            List<Credit> credits = GetCreditsForParcel(parameterArray);
                            foreach (Credit credit in credits)
                            {
                                insert = "INSERT INTO ParcelTaxCredits(PIN_18, CREDITAMOUNT, CREDITTYPE) " +
                                         "VALUES ('" + parameterArray[5] + "'," + credit.CreditAmount.ToString() + ",'" +
                                                       credit.CreditType + "')";

                                insertStatementList.Add(insert);
                            }
                        }

                        //ParcelTaxDeductions 
                        if (clientProps.ClientTables.Contains("ParcelTaxDeductions"))
                        {
                            List<Deduction> deductions = GetDeductionsForParcel(parameterArray);

                            foreach (Deduction deduction in deductions)
                            {
                                insert = "INSERT INTO ParcelTaxDeductions(PIN_18, DEDUCTIONAMOUNT, DEDUCTIONTYPE) " +
                                         "VALUES ('" + parameterArray[5] + "'," + deduction.DeductionAmount.ToString() + ",'" +
                                                       deduction.DeductionType.Replace("'", "''") + "')";

                                insertStatementList.Add(insert);
                            }
                        }

                        if (count % 10000 == 0)
                        {
                            Console.WriteLine("Processing Remittance records: " + count.ToString());
                        }

                        count++;
                        record = reader.ReadLine();
                    }

                    writer.WriteLine("Imported " + count.ToString() + " remittance records");
                    reader.Close();
                  
                    count = 1;

                    foreach (string insertStatement in insertStatementList)
                    {
                        try
                        {
                            sqlCommand.CommandText = insertStatement;
                            sqlCommand.ExecuteNonQuery();
                        }
                        catch
                        {
                            writer.WriteLine("Error executing statement: " + insertStatement);
                        }

                        if (count % 10000 == 0)
                        {
                            Console.WriteLine("Creating records: " + count.ToString());
                        }

                        count++;
                    }

                    Directory.Delete(extractDirectory, true);
                }

                System.Threading.Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                writer.WriteLine("Error: " + ex.StackTrace + ", " + ex.Message);
            }
            finally
            {
                if (sqlConnection != null)
                {
                  sqlConnection.Close();
                }
            }
        }

        private static List<Installment> GetInstallmentsForParcel(string[] parameterArray)
        {
            List<Installment> installments = new List<Installment>();

            //Spring
            double chargeAmount = Convert.ToDouble(parameterArray[23]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring installment",
                    ChargeAmount = chargeAmount,
                    AmountPaid = Convert.ToDouble(parameterArray[40]),
                    Balance = Convert.ToDouble(parameterArray[43])
                });

            chargeAmount = Convert.ToDouble(parameterArray[25]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring delinquent tax",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[26]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring delinquent penalty",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[27]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring penalty and fees",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[31]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring other assessment tax",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[33]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring other assessment delinquent tax",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[35]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring other assessment delinquent penalty",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[151]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Spring other assessment adjustment",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            //Fall
            chargeAmount = Convert.ToDouble(parameterArray[45]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Fall installment",
                    ChargeAmount = chargeAmount,
                    AmountPaid = Convert.ToDouble(parameterArray[55]),
                    Balance = Convert.ToDouble(parameterArray[57])
                });

            chargeAmount = Convert.ToDouble(parameterArray[46]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Fall penalty and fees",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[48]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Fall other assessment tax",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[153]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Fall other assessment adjustment",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            if (parameterArray[50].Trim().Length > 0)
            {
                chargeAmount = Convert.ToDouble(parameterArray[50]);
                if (chargeAmount > 0)
                    installments.Add(new Installment()
                    {
                        ChargeDescription = "Unpaid spring tax and delinquencies",
                        ChargeAmount = chargeAmount,
                        AmountPaid = 0,
                        Balance = 0
                    });
            }

            //Year total
            chargeAmount = Convert.ToDouble(parameterArray[58]);
            if (chargeAmount > 0)
            {
                double amountPaid = 0;
                if (parameterArray[66].Trim().Length > 0)
                    amountPaid = Convert.ToDouble(parameterArray[66]);

                installments.Add(new Installment()
                {
                    ChargeDescription = "Year total",
                    ChargeAmount = chargeAmount,
                    AmountPaid = amountPaid,
                    Balance = 0
                });
            }

            chargeAmount = Convert.ToDouble(parameterArray[60]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Year total delinquent tax",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[61]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Year total delinquent penalty",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[62]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Year total penalty and fees",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[152]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Year total other assessment adjustment",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            chargeAmount = Convert.ToDouble(parameterArray[154]);
            if (chargeAmount > 0)
                installments.Add(new Installment()
                {
                    ChargeDescription = "Year total other assessment tax",
                    ChargeAmount = chargeAmount,
                    AmountPaid = 0,
                    Balance = 0
                });

            return installments;
        }

        private static List<CreditRate> GetCreditRatesForParcel(string[] parameterArray)
        {
            List<CreditRate> creditRates = new List<CreditRate>();

            double creditRate = 0;
            if (double.TryParse(parameterArray[8], out creditRate))
            {
                if (creditRate > 0)
                    creditRates.Add(new CreditRate()
                    {
                        CreditRateType = "Homestead credit",
                        CreditRateValue = creditRate
                    });
            }

            creditRate = 0;
            if (double.TryParse(parameterArray[9], out creditRate))
            {
                if (creditRate > 0)
                    creditRates.Add(new CreditRate()
                    {
                        CreditRateType = "Replacement credit",
                        CreditRateValue = creditRate
                    });
            }

            creditRate = 0;
            if (double.TryParse(parameterArray[10], out creditRate))
            {
                if (creditRate > 0)
                    creditRates.Add(new CreditRate()
                    {
                        CreditRateType = "LOIT",
                        CreditRateValue = creditRate
                    });
            }

            return creditRates;
        }

        private static List<Credit> GetCreditsForParcel(string[] parameterArray)
        {
            List<Credit> credits = new List<Credit>();

            double creditAmount = 0;

            if (double.TryParse(parameterArray[11], out creditAmount))
            {
                if (creditAmount > 0)
                    credits.Add(new Credit()
                    {
                        CreditType = "LOIT",
                        CreditAmount = creditAmount
                    });
            }

            return credits;
        }

        private static List<Deduction> GetDeductionsForParcel(string[] parameterArray)
        {
            List<Deduction> deductions = new List<Deduction>();

            int index = 100;
            string exemptionName = parameterArray[index];
            while (exemptionName.Length > 0 && index <= 118)
            {
                deductions.Add(new Deduction()
                {
                    DeductionType = exemptionName,
                    DeductionAmount = Convert.ToDouble(parameterArray[index + 1])
                });

                index += 2;
                exemptionName = parameterArray[index];
            }

            return deductions;
        }

        private static List<Unit> GetUnitsForParcel(string[] parameterArray)
        {
            List<Unit> units = new List<Unit>();

            int unitCount = Convert.ToInt32(parameterArray[69]);
            int startIndex = 70;

            for (int i = startIndex; i < startIndex + 8 * unitCount; i += 8)
            {
                string unitAmountString = parameterArray[i + 2];
                if (unitAmountString.StartsWith("("))
                    unitAmountString = unitAmountString.Substring(1, unitAmountString.Length - 2);

                double unitAmount = Convert.ToDouble(unitAmountString);
                if (unitAmount > 0)
                    units.Add(new Unit()
                    {
                        UnitDescription = parameterArray[i],
                        UnitAmount = unitAmount
                    });
            }

            return units;
        }
    }
}
